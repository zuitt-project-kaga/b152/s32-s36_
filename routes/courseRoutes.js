const express = require("express");
const router = express.Router();
const courseControllers = require("../controllers/courseControllers");
const auth = require("../auth");
const {verify,verifyAdmin} = auth;

router.post("/",verify,verifyAdmin,courseControllers.addCourse);
router.get("/",courseControllers.getAllCourses);
router.get('/getSingleCourse/:id',courseControllers.getSingleCourse);
router.put('/:id',verify,verifyAdmin,courseControllers.updateCourse);
router.put('/archive/:id',verify,verifyAdmin,courseControllers.archiveCourse);
router.put('/activate/:id',verify,verifyAdmin,courseControllers.getActiveCourses);
router.get('/getActiveCourses',courseControllers.getActiveCourses);
router.get('/getInactiveCourses',verify,verifyAdmin,courseControllers.getInactiveCourses);
router.post('/lookForDocumentsOnName',courseControllers.lookForDocumentsOnName);
router.post('/lookForDocumentsOnPrice',courseControllers.lookForDocumentsOnPrice);
router.get('/getEnrollees/:courseid',verify,verifyAdmin,courseControllers.getEnrollees)


module.exports = router;