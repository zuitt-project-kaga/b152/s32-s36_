const express = require("express");
const router = express.Router();
const userControllers = require("../controllers/userControllers");

const auth = require("../auth");
const {verify,verifyAdmin} = auth;
// console.log(verify);

router.post("/",userControllers.registerUser);
router.get("/",userControllers.getAllUsers);
router.post("/login",userControllers.loginUser);
router.get('/getUserDetails',verify,userControllers.getUserDetails);
router.post('/checkEmailExists',userControllers.checkEmailExists);
router.put('/updateUserDetails',verify,verifyAdmin,userControllers.updateUserDetails);
router.put('/updateAdminStatus/:id',verify,verifyAdmin,userControllers.updateAdminStatus);
router.post('/enroll',verify,userControllers.enroll);
router.get('/getEnrollments',verify,userControllers.getEnrollments);

module.exports = router;