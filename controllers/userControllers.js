const User = require("../models/User");
const Course = require("../models/Course");
const bcrypt = require("bcrypt");
const auth = require("../auth");
// console.log(auth);

module.exports.registerUser = (req, res) => {
  console.log(req.body);
  // res.send("Test route");
  const hashedPW = bcrypt.hashSync(req.body.password, 10);
  //   console.log(hashedPW);
  let newUser = new User({
    firstName: req.body.firstName,
    lastName: req.body.lastName,
    email: req.body.email,
    mobileNo: req.body.mobileNo,
    password: hashedPW,
  });

  newUser
    .save()
    .then((user) => res.send(user))
    .catch((error) => res.send(error));
};

module.exports.getAllUsers = (req, res) => {
  User.find({})
    .then((result) => res.send(result))
    .catch((error) => res.send(error));
};

module.exports.loginUser = (req, res) => {
  //   console.log(req.body);
  User.findOne({ email: req.body.email })
    .then((foundUser) => {
      //   console.log(foundUser);
      if (foundUser === null) {
        return res.send("No User Found.");
      } else {
        // return res.send("User Found.");
        const isPasswordCorrect = bcrypt.compareSync(
          req.body.password,
          foundUser.password
        );
        // console.log(isPasswordCorrect);
        if (isPasswordCorrect) {
          // return res.send("We will generate a key.")
          // auth.createAccessToken();
          return res.send({ accessToken: auth.createAccessToken(foundUser) });
        } else {
          return res.send("Incorrect Password.");
        }
      }
    })
    .catch((err) => res.send(err));
};

module.exports.getUserDetails = (req, res) => {
  // console.log(req.user);
  // res.send("testing for verify");
  User.findById(req.user.id)
    .then((result) => res.send(result))
    .catch((err) => res.send(err));
};

module.exports.checkEmailExists = (req, res) => {
  //   User.findOne({ email: req.body.email })
  //     .then((result) => res.send(result))
  //     .catch((err) => res.send(err));
  User.findOne({ email: req.body.email })
    .then((result) => {
      if (result === null) {
        res.send("Email is available");
      } else {
        res.send("Email is already registered!");
      }
    })
    .catch((err) => res.send(err));
};

module.exports.updateUserDetails = (req, res) => {
  //   console.log(req.body);
  let updates = {
    firstName: req.body.firstName,
    lastName: req.body.lastName,
    mobileNo: req.body.mobileNo,
  };
  User.findByIdAndUpdate(req.user.id, updates, { new: true })
    .then((result) => res.send(result))
    .catch((err) => res.send(err));
};

module.exports.updateAdminStatus = (req, res) => {
  //   console.log(req.params.id);
  let updates = {
    isAdmin: req.body.isAdmin,
  };
  User.findOneAndUpdate(req.params.id, updates, { new: true })
    .then((result) => res.send(result))
    .catch((err) => res.send(err));
};

module.exports.enroll = async (req,res) => {
	//console.log("test enroll route")
	// console.log(req.user.id)
	// console.log(req.body.courseId)
	if(req.user.isAdmin) {
		return res.send("Action Forbidden")
	}
	
	let isUserUpdated = await User.findById(req.user.id).then(user => {

		let newEnrollment = {
			courseId: req.body.courseId
		}
		user.enrollments.push(newEnrollment);

		return user.save().then(user => true).catch(err => err.message)

	})

	if(isUserUpdated !== true) {
		return res.send({message: isUserUpdated})
	}

	let isCourseUpdated  = await Course.findById(req.body.courseId).then(course => {

		//console.log(course);
		let enrollee = {
			userId: req.user.id
		}
		course.enrollees.push(enrollee);
		return course.save().then(course => true).catch(err => err.message)
	})

	console.log(isCourseUpdated);
	if(isCourseUpdated !== true){
		return res.send({message: isCourseUpdated})
	}

	if(isUserUpdated && isCourseUpdated){
		return res.send({message: "Enrolled Successfully."})
	}

}

module.exports.getEnrollments = (req,res) =>{
    User.findById(req.user.id).then(appliedUser=>{
        if(appliedUser.enrollments.length > 0){
            res.send(appliedUser.enrollments);
        }else{
            res.send("Hey!Hey!Hey,YO! No enrolled course!");
        }
    }).catch(err=>res.send(err))
}

