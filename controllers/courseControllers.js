const Course = require("../models/Course");

module.exports.addCourse = (req, res) => {
  let newCourse = new Course({
    name: req.body.name,
    description: req.body.description,
    price: req.body.price,
  });
  newCourse
    .save()
    .then((result) => res.send(result))
    .catch((error) => res.send(error));
};

module.exports.getAllCourses = (req, res) => {
  Course.find({})
    .then((result) => res.send(result))
    .catch((error) => res.send(error));
};

module.exports.getSingleCourse = (req, res) => {
  Course.findById(req.params.id)
    .then((result) => res.send(result))
    .catch((err) => res.send(err));
};

module.exports.updateCourse = (req, res) => {
  let updates = {
    name: req.body.name,
    description: req.body.price,
    price: req.body.price,
  };
  Course.findByIdAndUpdate(req.params.id, updates, { new: true })
    .then((updatedCourse) => res.send(updatedCourse))
    .catch((err) => res.send(err));
};

module.exports.archiveCourse = (req, res) => {
  let updates = {
    isActive: false,
  };
  Course.findByIdAndUpdate(req.params.id, updates, { new: true })
    .then((archiveCourse) => res.send(archiveCourse))
    .catch((err) => res.send(err));
};

module.exports.activateCourse = (req, res) => {
  let updates = {
    isActive: true,
  };
  Course.findByIdAndUpdate(req.params.id, updates, { new: true })
    .then((activateCourse) => res.send(activateCourse))
    .catch((err) => res.send(err));
};

module.exports.getActiveCourses = (req, res) => {
  Course.find({ isActive: true })
    .then((result) => res.send(result))
    .catch((err) => res.send(err));
};

module.exports.getInactiveCourses = (req, res) => {
  Course.find({ isActive: false })
    .then((result) => res.send(result))
    .catch((err) => res.send(err));
};

module.exports.lookForDocumentsOnName = (req, res) => {
//   Course.findOne({name: req.body.name })
  Course.find({name: {$regex: req.body.name, $options:'$i' } })
    .then((result) => {
      if (result.length === 0) {
        res.send("No Course found");
      } else {
        res.send(result);
      }
    })
    .catch((err) => req.send(err));
};

module.exports.lookForDocumentsOnPrice = (req, res) => {
  Course.find({ price: req.body.price })
    .then((result) => {
      if (result.length === 0) {
        res.send("No Course found");
      } else {
        res.send(result);
      }
    })
    .catch((err) => req.send(err));
};

module.exports.getEnrollees = (req,res) =>{
    Course.findById(req.params.courseid).then(appliedCourse=>{
        if(appliedCourse.enrollees.length > 0){
            res.send(appliedCourse.enrollees);
        }else{
            res.send("Register now ahahahahah!")
        }
    }).catch(err=>res.send(err));
}